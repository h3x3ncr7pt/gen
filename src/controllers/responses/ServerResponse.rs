use serde::{Deserialize,Serialize};
#[derive(Deserialize,Serialize)]
pub struct ServerResponse {
    pub data : Option<serde_json::Value>,
    pub code : Option<i32>,
}
impl ServerResponse {
    pub fn default() -> ServerResponse{
    ServerResponse {
        data : Some(serde_json::from_str("{}").unwrap()),
        code : Some(54),
        }
    }
}
