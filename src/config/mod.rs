use actix_web::web;
use sqlx::{MySql, Pool};
use sqlx::mysql::MySqlPoolOptions;
use sqlx::pool::PoolOptions;
use crate::HttpResponse;
use crate::controllers::{ Auth_Controller };

//DB Singleton
//pub(crate) static CONNECTION: Pool<MySql> = MySqlPoolOptions::new().max_connections(10).connect(&"mysql://root:mama.123@localhost/users_db").await?;

#[derive(Clone)]
pub struct DBState {
    pub db : Pool<MySql>
}

pub fn config_routes(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::scope("/api/v1")
            .service(
                web::resource("/auth/login")
            
                .route(web::post().to(Auth_Controller::loginUser))
                
                
                
            )
        );
}

