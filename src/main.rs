use actix_web::{App, HttpResponse, HttpServer, web};
use sqlx::mysql::MySqlPoolOptions;

mod config;
mod controllers;
mod models;
mod services;
#[actix_web::main]
async fn main() -> std::io::Result<()>{
    let db_pool = config::DBState {
        db : MySqlPoolOptions::new().max_connections(10).connect(&"mysql://root:mama.123@localhost/users_db").await.unwrap()
    };
    HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .configure(config::config_routes)

        })
        .bind("127.0.0.1:8080")?
        .run()
        .await
}

#[cfg(test)]
mod tests{
    use actix_web::{App, test};
    use actix_web::dev::{Body, ResponseBody};
    use actix_web::http::header;
    use crate::config::config_routes;
    use crate::models;
    use crate::controllers;

    #[actix_rt::test]
    async fn test_loginUser(){
        let mut app = test::init_service(
            App::new()
                .configure(config_routes)
        ).await;
        let mut resp = test::TestRequest::post()
            .uri("/api/v1/auth/login")
            .set_json(&controllers::requests::loginrequest::LoginRequest::default())
            .send_request(&mut app)
            .await;
        //Your assertion here
        assert_eq!(resp.take_body().as_ref().unwrap(),&Body::from("This endpoint is for user login - loginUser"));
    }
}
